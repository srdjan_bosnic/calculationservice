﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using Calculation;
using DAL;

using Mip2.Logger;

using MySql.Data.MySqlClient;

namespace CalculationService
{
    public partial class CalculationService : ServiceBase
    {
        #region Fields
        
        private readonly long _interval;
        private Timer _oTimer;
        
        #endregion

        #region Contructors

        public CalculationService()
        {
            var serviceTimeInterval = int.Parse(ConfigurationManager.AppSettings["serviceTimeInterval"]);
            _interval = serviceTimeInterval;
            InitializeComponent();
            InitializeService();
        }

        #endregion

        #region Public Methods and Operators+

        public void OnDebug()
        {
            OnStart(null);
        }

        #endregion

        #region Protected Members

        protected override void OnStart(string[] args)
        {
            // EventLog.WriteEntry("Dings System Monitor Started.");
            this._oTimer.Enabled = true;
            this._oTimer.Start();
        }

        protected override void OnStop()
        {
            // EventLog.WriteEntry("Dings System Monitor Stopped.");
            this._oTimer.Enabled = false;
            this._oTimer.Stop();
            File.Create(AppDomain.CurrentDomain.BaseDirectory + "OnStop.txt");
        }

        protected override void OnPause()
        {
            // EventLog.WriteEntry("Dings System Monitor Paused.");
        }

        protected override void OnContinue()
        {
            // EventLog.WriteEntry("Dings System Monitor Resumed.");
        }

        #endregion

        #region Private Members

        private void InitializeService()
        {
            this._oTimer = new Timer(this._interval);
            this._oTimer.Enabled = true;
            this._oTimer.AutoReset = true;
            this._oTimer.Start();
            this._oTimer.Elapsed += this.OTimerElapsed;
        }

        private void OTimerElapsed(object sender, ElapsedEventArgs e)
        {
            //MipLogger.LogVerbose("Entered CalculationService.OTimerElapsed");

            // for debbuging
            //_oTimer.Stop();

            // create calculation engine
            using (var calculationManager = new CalculationManager())
            {
                try
                {
                    calculationManager.Run();
                }
                catch (Exception error)
                {
                    if (error.InnerException is MySqlException)
                    {
                        //todo mm pokusaj jos par puta ako je konekcija pukla
                    }
                    else
                    {
                        MipLogger.LogError(error);
                    }
                }
            }
            //MipLogger.LogVerbose("Leaving CalculationService.OTimerElapsed");
        }

        #endregion
    }
}