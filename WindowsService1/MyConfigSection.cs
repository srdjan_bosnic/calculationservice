﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationService
{
    public class MyConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("rowLimit")]
        public string RowLimit
        {
            get { return (string) this["rowLimit"]; }
            set { this["rowLimit"] = value; }
        }
    }
}
