﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace CalculationService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {   

        private const string LogName = "My Service Log";

        public ProjectInstaller()
        {
            InitializeComponent();
            EventLogInstaller EventLogInstall = null;

            foreach (Installer I in this.serviceInstaller1.Installers)
            {
                EventLogInstall = I as EventLogInstaller;

                if (EventLogInstall != null)
                {
                    EventLogInstall.Log = LogName;
                    EventLogInstall.UninstallAction = UninstallAction.NoAction;
                    break;
                }
            } 
        }

        private void serviceInstaller2_AfterInstall(object sender, InstallEventArgs e)
        {

        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            new ServiceController(serviceInstaller1.ServiceName).Start();
        }
    }
}
