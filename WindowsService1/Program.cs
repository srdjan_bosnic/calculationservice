﻿using System.ServiceProcess;
using System.Threading;

namespace CalculationService
{
    internal static class Program
    {
        #region Private Members

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        private static void Main()
        {
        #if DEBUG
            var myService = new CalculationService();
            myService.OnDebug();
            Thread.Sleep(Timeout.Infinite);

        #else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new CalculationService()  
            };
            ServiceBase.Run(ServicesToRun);
        #endif
        }

        #endregion
    }
}