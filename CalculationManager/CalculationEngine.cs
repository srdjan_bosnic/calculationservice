﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.Ef;
using Mip2.Logger;

namespace Calculation
{
    public class CalculationEngine  
    {
        #region Fields
        //private AccurateEntities _dbContext = new AccurateEntities();
        private readonly DALManager _getterManager;
        #endregion

        //#region IDisposable
        //public void Dispose()
        //{
        //    //_dbContext.Dispose();
        //}
        //#endregion

        #region Calculation Members

        public CalculationEngine(DALManager getterManager)
        {
            _getterManager = getterManager;
        }

        //---CALCULATION---

        public decimal CalculateScoreForParameterList(CreditRequest creditRequest, IEnumerable<Parameter> parameterList)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateScoreForParameterList with CreditRequestID: " + creditRequest.ID);
            decimal score = 0;
            try
            {
                foreach (var parameter in parameterList)
                {
                    //return CreditRequestParameterValue for current Parameter and CreditRequestID
                    var creditRequestParameterValue = _getterManager.GetCreditRequestParameterValueForParameter(creditRequest, parameter);

                    //recalc value for CreditRequestParameterValue
                    score = score + CalculateParameterRecalculatedValue(creditRequest, parameter, creditRequestParameterValue);
                }
                MipLogger.LogInfo("Score, apart of constant, is successfully calculated. Value is: " + score);

                //add constant
                var constant = _getterManager.GetConstants(creditRequest.wSegmentCreditType.Model);
              
                score = score + (constant[0].Beta == null ? 0 : constant[0].Beta.Value *
                         constant[0].BetaWeight == null ? 1 : constant[0].BetaWeight.Value);

                MipLogger.LogInfo("Score is successfully calculated. Value is: " + score);
                MipLogger.LogInfo("Leaving CalculationEngine.CalculateScoreForParameterList.");
                return score;
            }
            catch (Exception)
            {
                MipLogger.LogError("Score calculation for CreditRequestID: " + creditRequest.ID + " has been stopped" + Environment.NewLine + "Leaving CalculationEngine.CalculateScoreForParameterList");
                throw;
            }
        }
        public int CalculateRequestStatusForCreditRequest(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateRequestStatusForCreditRequest with CreditRequestID: " + creditRequest.ID);
            try
            {
                var requestStatus = _getterManager.GetRequestStatusForCreditRequest(creditRequest);

                MipLogger.LogInfo("Leaving CalculationEngine.CalculateRequestStatusForCreditRequest, returning request status value: " + requestStatus);
                return requestStatus;
            }
            catch (Exception)
            {
                MipLogger.LogError("Calculating request status for CreditRequest.ID: " + creditRequest.ID + " has been stopped");
                throw;
            }
        }
        public decimal CalculateRecalculatedValueForDiscreteParameter(Parameter parameter, CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateRecalculatedValueForDiscreteParameter for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
            try
            {
                //todo sb ako upit nista ne vraca da li onda dobijam null? 
                {
                    var singleParameterAttribute = _getterManager.GetparameterAttributeForDiscreteParameterType(parameter, creditRequestParameterValue);
                    //return RecalculatedValue
                    var result = (singleParameterAttribute.Beta.Value * singleParameterAttribute.BetaWeight.Value); //Beta and BetaWeight are already checked for not to be null
                    MipLogger.LogInfo("Leaving CalculationEngine.CalculateRecalculatedValueForDiscreteParameter. Returning value: " + result);
                    return result;
                }
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate recalculated value for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
                MipLogger.LogInfo("Leaving CalculationEngine.CalculateRecalculatedValueForDiscreteParameter ");
                throw;
            }
        }
        public decimal CalculateRecalculatedValueForDiscretizedContinuousParameter(CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateRecalculatedValueForDiscretizedContinuousParameter for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
            try
            {
                {
                    var singleParameterAttribute = _getterManager.GetBelongingParameterAttribute(creditRequestParameterValue);

                    //return RecalculatedValue
                    //todo sb pogledaj sta ovde vraca za result sa razlicitim vrednostima beta
                    //todo alex zasto obavezna polja nisu takva i u bazi
                    var result = (singleParameterAttribute.Beta * singleParameterAttribute.BetaWeight); //Beta and BetaWeight are already checked for not to be null
                    MipLogger.LogInfo("Leaving CalculationEngine.GetRecalculatedValueForDiscreteParameter. Returning value: " + result);
                    return Convert.ToDecimal(result);
                }
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate recalculated value for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
                throw;
            }
        }
        public decimal CalculateRecalculatedValueForContinuousParameter(CreditRequest creditRequest, Parameter parameter, CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateRecalculatedValueForContinuousParameter for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
            try
            {
                {
                    //TODO probaj da sredis upit zato sto ovako mesa left outer i inner join.

                    var singleParameterAttributeList = _getterManager.GetparameterAttributeForDiscreteParameterType(creditRequest, parameter);
                    //must return one row
                    switch (singleParameterAttributeList.Count())
                    {
                        case 0:
                            MipLogger.LogError("Missing ParameterAttribute");
                            throw new Exception();
                        case 1:
                            break;
                        default:
                            MipLogger.LogError("Multiple entry for ParameterAttribute");
                            throw new Exception();
                    }

                    //return RecalculatedValue
                    var result = singleParameterAttributeList[0].Value;
                    MipLogger.LogInfo("Leaving CalculationEngine.CalculateRecalculatedValueForContinuousParameter. Returning value: " + result);
                    return result;
                }
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate recalculated value for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
                throw;
            }
        }
        public decimal CalculateProbabilityOfDefault(decimal score)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateProbabilityOfDefault for score: " + score);

            try
            {
                //math.exp prima samo double vrednost, zato se radi convert ToDouble pa covert ToDecimal
                var pd = 1 / (1 + Convert.ToDecimal(Math.Exp(-Convert.ToDouble(score))));
                MipLogger.LogInfo("Leaving CalculationEngine.CalculateProbabilityOfDefault, returning value: " + pd);
                return pd;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate PD for score: " + score + Environment.NewLine + "Leaving CalculationEngine.CalculateProbabilityOfDefault");
                throw;
            }
        }
        public decimal CalculateParameterRecalculatedValue(CreditRequest creditRequest, Parameter parameter, CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateParameterRecalculatedValue with CreditRequestID: " +
                creditRequest.ID + ", ParameterID: " + parameter.ID + " and CreditRequestParameterValueID: " + creditRequestParameterValue.ID);

            try
            {
                //switch za razalicite tipove parametra (diskretni, diskretizovani, kontinualni, const)
                decimal recalculatedValue;

                switch (parameter.ParameterTypeID)
                {
                    //Discrete
                    case 1:
                        recalculatedValue = CalculateRecalculatedValueForDiscreteParameter(parameter, creditRequestParameterValue);
                        break;
                    //DiscretizedContinuous
                    case 2:
                        recalculatedValue = CalculateRecalculatedValueForDiscretizedContinuousParameter(creditRequestParameterValue);
                        break;
                    //Continuous
                    case 3:
                        recalculatedValue = CalculateRecalculatedValueForContinuousParameter(creditRequest, parameter, creditRequestParameterValue);
                        break;
                    //else
                    default:
                        throw new Exception();
                }
                creditRequestParameterValue.RecalculatedValue = recalculatedValue;
                MipLogger.LogInfo("Recalculated value for CreditRequestParameterValue.ID " + creditRequestParameterValue.ID + " is: " + recalculatedValue);
                MipLogger.LogInfo("Leaving CalculationEngine.CalculateParameterRecalculatedValue");
                return recalculatedValue;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to recalculate value for CreditRequestParameterValue.ID: " + creditRequestParameterValue.ID +
                    Environment.NewLine + "Leaving CalculationEngine.CalculateParameterRecalculatedValue");
                throw;
            }
        }

        #endregion
    }
}
