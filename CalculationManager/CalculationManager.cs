﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.Ef;
using Mip2.Logger;
using MySql.Data.MySqlClient;
using Validation;
using System.Configuration;
 

namespace Calculation
{
    public class CalculationManager : IDisposable
    {
        #region Fields

        private readonly DALManager _getterManager = new DALManager();
        private CreditRequest _creditRequest;
        readonly Stopwatch _stopwatch = new Stopwatch();

        #endregion

        public void Run()
        {
            // ovo bi prepunilo log, mozda prebaciti u neku drugu vrstu loga?
            //MipLogger.LogVerbose("Entered CalculationManager.Run");
            try
            {
                using (_getterManager)
                {
                    //todo sb testiraj pucanje servisa u toku kalkulacije
                    // get limited number of credit requests (taken from app.config) that needs to be calculated, request with status id = 1
                    var rowLimit = int.Parse(ConfigurationManager.AppSettings["rowLimit"]);

                    //only one instance of service is allowed, so if there is creditRequest in Progress it's because last service broke in the middle of execution. Set that CreditRequest to 1
                    //todo mm da li da disposeujem getter i ugasim service ako do ovoga dodje , ili samo da vratim flag na 1?
                    var creditRequestInProgressList = _getterManager.GetTopCreditRequestsByStatusId(2, rowLimit);
                    if (creditRequestInProgressList.Any())
                    {
                        foreach (var creditRequestInProgress in creditRequestInProgressList)
                        {
                            creditRequestInProgress.CalculationStatusID = 1;
                        }
                        return;
                    }

                    var creditRequestList = _getterManager.GetTopCreditRequestsByStatusId(1, rowLimit);



                    foreach (var creditRequest in creditRequestList)
                    {
                        if (creditRequest != null)
                        {

                            _creditRequest = creditRequest;

                            _stopwatch.Start();
                            //invoke calc for creditRequestsList
                            //todo sb pogledaj sta se desava kada nema  nista za racunanje, sta vraca creditrequest.
                            InvokeCalculationForCreditRequest();
                            _stopwatch.Stop();
                            MipLogger.LogVerbose("Leaving CalculationManager.Run. Time elapsed: " + _stopwatch.Elapsed);
                        }
                    }
                }
                // ovo bi prepunilo log 
                //MipLogger.LogVerbose("Leaving CalculationManager.Run");
            }
            catch (Exception)
            {
                //UNDO, set creditRequest status to error (creating new dbContext)

                if (_getterManager != null) _getterManager.Dispose();

                using (var getterManager = new DALManager())
                {
                    getterManager.SetCalculationStatus(_creditRequest.ID, 4);
                }

                _stopwatch.Stop();
                MipLogger.LogVerbose("Leaving CalculationManager.Run. Time elapsed: " + _stopwatch.Elapsed);
                throw;
            }
        }

        public void InvokeCalculationForCreditRequest()
        {
            MipLogger.LogInfo("**** Entered CalculationManager.InvokeCalculationForCreditRequest with CreditRequestID: " + _creditRequest.ID + " ****");

            try
            {
                //set SetCalculationStatus to "InProgress"
                _getterManager.SetCalculationStatus(_creditRequest.ID, 2);

                //validate CreditRequest
                var validation = new Validation.Validation(_getterManager);
                validation.ValidationManager(_creditRequest);


                //set creditRequest to inProgress status
                _getterManager.SetCalculationStatus(_creditRequest.ID, 2);

                //start calculation for CreditRequest
                StartCalculation(_creditRequest);

                //commit previous changes
                _getterManager.CommitCalculationResults(_creditRequest);

                // next creditRequest itt
            }

            catch (Exception caluclationException)
            {
                if (caluclationException.InnerException is MySqlException)
                
                    //TODO: MM handle network disconection (etc try for 15 sec then quit)
                    MipLogger.LogError(caluclationException.InnerException);
               
                MipLogger.LogError("Calculation for CreditRequestId: " + _creditRequest.ID + " has been stopped." + Environment.NewLine + "Leaving CalculationManager.InvokeCalculationForCreditRequest");
                throw;
            }
            MipLogger.LogInfo("Leaving CalculationManager.InvokeCalculationForCreditRequest");
        }
        public void StartCalculation(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered CalculationManager.StartCalculation with CreditRequestID: " + creditRequest.ID);

            try
            {
                //returns model
                Model model = _getterManager.GetModelForCreditRequest(creditRequest);

                //returns param list for selected model, apart of constant
                var parameterList = _getterManager.GetParameterListForModel(model);

                //calculate score (including constant)
                var calculationEngine = new CalculationEngine(_getterManager);
                var score = calculationEngine.CalculateScoreForParameterList(creditRequest, parameterList);
                creditRequest.Score = score;

                //calculate PD
                creditRequest.PD = calculationEngine.CalculateProbabilityOfDefault(score);

                //Calculate RequestStatus
                calculationEngine.CalculateRequestStatusForCreditRequest(creditRequest);
            }
            catch (Exception)
            {
                MipLogger.LogError("Calculation for CreditRequest.ID: " + creditRequest.ID + " has been stoped." + Environment.NewLine + "Leaving CalculationManager.StartCalculation");
                throw;
            }
            MipLogger.LogInfo("Leavinng CalculationManager.StartCalculation");
        }

        public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    // NOTE: Leave out the finalizer altogether if this class doesn't 
    // own unmanaged resources itself, but leave the other methods
    // exactly as they are. 
    ~CalculationManager() 
    {
        // Finalizer calls Dispose(false)
        Dispose(false);
    }
    // The bulk of the clean-up code is implemented in Dispose(bool)
    protected virtual void Dispose(bool disposing)
    {
        if (disposing) 
        {
            // free managed resources
            //if (managedResource != null)
            //{
            //    managedResource.Dispose();
            //    managedResource = null;
            //}
        }
        // free native resources if there are any.
        //if (nativeResource != IntPtr.Zero) 
        //{
        //    Marshal.FreeHGlobal(nativeResource);
        //    nativeResource = IntPtr.Zero;
        //}
    }
    }
}
