﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.Ef;
using Microsoft.CSharp.RuntimeBinder;
using Mip2.Logger;
using Validation.Validation_engine;

namespace Validation
{
    public class Validation : IDisposable
    {
        private readonly DALManager _getterManager;

        public Validation(DALManager getterManager)
        {
            _getterManager = getterManager;
        }

        public void ValidationManager(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered Validation.ValidationManager, for CreditRequest.ID: " + creditRequest.ID);
            try
            {
                var validationEngine = new OtherValidationEngines(_getterManager);
                var paramValidationEngine = new ParameterValidationEngine(_getterManager);

                //validate creditRequest
                validationEngine.IsCreditRequestValid(creditRequest);

                //validate model
                validationEngine.IsModelValid(creditRequest.wSegmentCreditType.Model);

                //validate parameters
                paramValidationEngine.ParameterValidationManager(creditRequest.wSegmentCreditType.Model);
            }
            catch (Exception)
            {
                MipLogger.LogError("Validation for CreditRequest.ID: " + creditRequest.ID + " was unsuccessfull." + Environment.NewLine + "Leaving Validation.ValidationManager");
                throw;
            }
            MipLogger.LogInfo("Leaving Validation.ValidationManager");
        }
        public void Dispose()
        {
        }
    }
}
