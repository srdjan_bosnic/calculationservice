﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.Ef;
using Mip2.Logger;

namespace Validation
{
    class ParameterValidationEngine
    {
        private readonly DALManager _getterManager;

        public ParameterValidationEngine(DALManager getterManager)
        {
            _getterManager = getterManager;
        }

        public void ParameterValidationManager(Model model)
        {
            MipLogger.LogInfo("Entered ParameterValidationEngine.ParameterValidationManager, in order to check parameters for Model.ID: " + model.ID);

            try
            {
                var parameterList = _getterManager.GetParameterListForModel(model);

                foreach (var parameter in parameterList)
                {
                    IsParameterValid(parameter);
                }

                MipLogger.LogInfo("Leaving ParameterValidationEngine.ParameterValidationManager");
            }
            catch (Exception)
            {
                MipLogger.LogError("Parameters for Model.ID: " + model.ID + " are not valid." + Environment.NewLine + "Leaving ParameterValidationEngine.ParameterValidationManager");
                throw;
            }
        }

        public void IsParameterValid(Parameter parameter)
        {
            MipLogger.LogInfo("Entered ParameterValidationEngine.IsParameterValid with ParameterID: " + parameter.ID);
            try
            {
                switch (parameter.ParameterTypeID)
                {
                    //Discrete parameter type
                    case 1:
                        IsValidDiscreteParameter(parameter);
                        break;
                    //DiscretizedContinuous parameter type
                    case 2:
                        IsValidDiscretizedContinuousParameter(parameter);
                        break;
                    //Continuous parameter type
                    case 3:
                        IsValidContinuousParameter(parameter);
                        break;
                    //else
                    default:
                        MipLogger.LogError("Unallowed value for parameter.ParameterTypeID");
                        throw new Exception();
                }
                MipLogger.LogInfo("Leaving IsParameterValid.IsParameterValid");
            }

            catch (Exception)
            {
                MipLogger.LogError("ParameterID: " + parameter.ID + " is not valid." + Environment.NewLine + "Leaving ParameterValidationEngine.IsParameterValid");
                throw;
            }
        }
        public void IsValidDiscreteParameter(Parameter parameter)
        {
            MipLogger.LogInfo("Entered ParameterValidationEngine.IsValidDiscreteParameter with ParameterID: " + parameter.ID);
            try
            {
                //must be at least 2 intervals
                ////todo sb test:
                //IGenericDataRepository<ParameterAttribute> repository = new GenericDataRepository<ParameterAttribute>();
                //IList<ParameterAttribute> parameterAttributes = repository.GetAll(pa => pa.ParameterID == parameter.ID);

                //get list  of parameter atributes
                var parameterAttributeList = _getterManager.GetParameterAttributesForParameter(parameter);

                //if less than 2 parameterAttribute
                if (parameterAttributeList.Count() < 2)
                {
                    MipLogger.LogError("Discrete type of parameters requires at least 2 intervals defined in ParameterAttribute");
                    throw new Exception();
                }

                //check mandatory fields
                foreach (var parameterAttribute in parameterAttributeList)
                {
                    CheckMandatoryFields(parameterAttribute);
                }

                MipLogger.LogInfo("Leaving ParameterValidationEngine.IsValidDiscreteParameter");
            }
            catch (Exception)
            {
                MipLogger.LogError("Discrete Parameter.ID: " + parameter.ID + " is not valid." + Environment.NewLine + "Leaving ParameterValidationEngine.IsValidDiscreteParameter.");
                throw;
            }
        }

        public void IsValidDiscretizedContinuousParameter(Parameter parameter)
        {
            MipLogger.LogInfo("Entered ParameterValidationEngine.isValidDiscretizedContinuousParameter with ParameterID: " + parameter.ID);

            try
            {
                //todo proveri da li ovo sortira kako treba, ako ne, koristi upit ispod

                var parameterAttributeList = _getterManager.GetParameterAttributesForParameter(parameter).OrderBy((w => w.LowerBound)).ToList();

                //must be at least 2 intervals
                if (parameterAttributeList.Count() < 2)
                {
                    MipLogger.LogError(
                        "DiscretizedContinuous type of parameters requires at least 2 intervals defined in ParameterAttribute.");
                    throw new Exception();
                }

                //some additional check:
                foreach (var parameterAttribute in parameterAttributeList)
                {
                    var currentIndex = parameterAttributeList.IndexOf(parameterAttribute);

                    //check mandatory fields
                    CheckMandatoryFields(parameterAttribute);

                    //check if inteval is overlapping
                    if (currentIndex != parameterAttributeList.Count() - 1) //check if this is not last foreach member
                    {
                        if (parameterAttribute.UpperBound > parameterAttributeList[currentIndex + 1].LowerBound)
                        {
                            MipLogger.LogError("Bound overlapping for ParameterID: " + parameter.ID +
                                               ". Check ParameterAttributeID: " + parameterAttribute.ParameterID);
                            throw new Exception();
                        }
                        //uncovered interval gap
                        if (parameterAttribute.UpperBound < parameterAttributeList[currentIndex + 1].LowerBound)
                        {
                            MipLogger.LogError("Uncovered interval gep for parameterId: " + parameter.ID +
                                               "Check ParameterAttributeID: " + parameterAttribute.ParameterID);
                            throw new Exception();
                        }
                    }
                    //if LowerBound > UpperBound?
                    IsBoundOrderCorrect(parameterAttribute);

                }
                MipLogger.LogInfo("Leaving ParameterValidationEngine.isValidDiscretizedContinuousParameter");
            }
            catch (Exception)
            {
                MipLogger.LogError("DiscretizedContinuous Parameter.ID: " + parameter.ID + " is not valid." + Environment.NewLine + "Leaving ParameterValidationEngine.IsValidDiscretizedContinuousParameter.");
                throw;
            }
        }

        public void IsValidContinuousParameter(Parameter parameter)
        {
            MipLogger.LogInfo("Entering ParameterValidationEngine.IsValidContinuousParameter for ParameterID: " + parameter.ID);

            try
            {
                var parameterAttributesList = _getterManager.GetParameterAttributesForParameter(parameter);

                //check mandatory fields
                CheckMandatoryFields(parameterAttributesList[0]);

                //must be only one:
                if (parameterAttributesList.Count() != 1)
                {
                    MipLogger.LogError("Continuous type of parameters must have single entry in ParameterAtribute.");
                    throw new Exception();
                }
                //LowerBound > UpperBound?
                IsBoundOrderCorrect(parameterAttributesList[0]);
           
                MipLogger.LogInfo("Leaving ParameterValidationEngine.IsValidContinuousParameter");
            }
            catch (Exception)
            {
                MipLogger.LogError("Continuous parameter Parameter.ID: " + parameter.ID + " is not valid." + Environment.NewLine + "Leaving ParameterValidationEngine.IsValidContinuousParameter");
                throw;
            }
        }

        public bool IsBoundOrderCorrect(ParameterAttribute parameterAttribute)
        {
            MipLogger.LogInfo("Entered ParameterValidationEngine.IsBoundOrderCorrect with ParameterAttribute.ParameterID: "
                              + parameterAttribute.ParameterID + " and ParameterAttribute.AttributeID: " + parameterAttribute.AttributeID);
            try
            {
                if (parameterAttribute.LowerBound > parameterAttribute.UpperBound)
                {
                    MipLogger.LogError("Invalid bounds order. LowerBound must be smaller or equal than UpperBound");
                    throw new Exception();
                }
                MipLogger.LogInfo("Leaving ParameterValidationEngine.IsBoundOrderCorrect");
                return true;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unsuccessfull lower and upper bound check");
                MipLogger.LogInfo("Leaving ParameterValidationEngine.IsBoundOrderCorrect");
                throw;
            }
        }

        public void CheckMandatoryFields(ParameterAttribute parameterAttribute)
        {
            MipLogger.LogInfo("Entered ValidationEngine.CheckMandatoryFields for ParameterAttribute.AttributeID: " +
                              parameterAttribute.AttributeID + " and ParameterAttribute.ParameterID: " + parameterAttribute.ParameterID);

            try
            {
                //check ParameterAttribute mandatory fields
                switch (parameterAttribute.Parameter.ParameterTypeID)
                {
                    //Discrete
                    case 1:
                        if (parameterAttribute.Beta == null | parameterAttribute.BetaWeight == null)
                        {
                            MipLogger.LogError("For discrete parameters fields Beta and BetaWeight are mandatory");
                            throw new Exception();
                        }
                        break;
                    //DiscretizedContinuous
                    case 2:
                    //Continuous
                    case 3:
                        if (parameterAttribute.Beta == null | parameterAttribute.BetaWeight == null | parameterAttribute.LowerBound == null | parameterAttribute.UpperBound == null)
                        {
                            MipLogger.LogError(
                                "For DiscretizedContinuous and Continuous parameters fields Beta, BetaWeight, LowerBound and UpperBound are mandatory");
                            throw new Exception();
                        }
                        break;
                    default:
                        MipLogger.LogError("Unallowed value for Parameter.ParameterTypeID");
                        throw new Exception();
                }
                MipLogger.LogInfo("Leaving ParameterValidationEngine.CheckMandatoryFields");
            }
            catch (Exception)
            {
                MipLogger.LogError("Mandatory fields check failed for parameter attribute with ParameterAttribute.AttributeID: " +
                    parameterAttribute.AttributeID + " and ParameterAttribute.ParameterID: " + parameterAttribute.ParameterID +
                    Environment.NewLine + "Leaving ParameterValidationEngine.CheckMandatoryFields");
                throw;
            }
        }

    }
}
