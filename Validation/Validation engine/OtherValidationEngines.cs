﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.Ef;
using Mip2.Logger;

namespace Validation.Validation_engine
{
    class OtherValidationEngines
    {
        private readonly DALManager _getterManager;

        public OtherValidationEngines(DALManager getterManager)
        {
            _getterManager = getterManager;
        }

        public void IsCreditRequestValid(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered ValidationEngine.IsCreditRequestValid with CreditRequestID: " + creditRequest.ID);

            //todo: alex dodati neku kontrolu na nivou kredita (da li postoji navedeni user, creditsegment...)
            try
            {
                //check CreditRequestParameterValue list
                IsCreditRequestParameterValueListComplete(creditRequest);

                IsValidCriteriaBound(_getterManager.GetCriteriaBoundForCreditRequest(creditRequest));

                //is wSegmentCreditType enabled
                if (creditRequest.wSegmentCreditType.Enabled == false)
                {
                    MipLogger.LogError("SegmentCreditType is not enabled");
                }

            }
            catch (Exception)
            {
                MipLogger.LogError("CreditRequest.ID: " + creditRequest.ID + " is not valid.");
                MipLogger.LogError("Leaving ValidationEngine.IsCreditRequestValid");
                throw;
            }
        }
        public void IsModelValid(Model model)
        {
            MipLogger.LogInfo("Entered ValidationEngine.IsModelValid with ModelID: " + model.ID);

            try
            {

                //check is there is one constant per model
                switch (_getterManager.GetConstants(model).Count())
                {
                    //missing const
                    case 0:
                        MipLogger.LogError("Constant is missing");
                        throw new Exception();
                    //one const, correct
                    case 1:
                        MipLogger.LogInfo("One constant found");
                        break;
                    //multiple consts
                    default:
                        MipLogger.LogError("Multiple constants found");
                        throw new Exception();
                }

                //must have at least 1 param (apart of const)
                if (!_getterManager.GetParameterListForModel(model).Any())
                {
                    throw new Exception("No parameters found");
                }

                //check model parameters
                var parameterValidationEngine = new ParameterValidationEngine(_getterManager);
                parameterValidationEngine.ParameterValidationManager(model);

            }
            catch (Exception)
            {
                MipLogger.LogError("Unsuccessful validation for ModelID: " + model.ID + Environment.NewLine + "Leaving ValidationEngine.IsModelValid.");
                throw;
            }
        }
        public void IsValidCriteriaBound(CriteriaBound criteriaBound)
        {
            MipLogger.LogInfo("Entered ValidationEngine.IsValidCriteriaBound with CriteriaBound.ID: " + criteriaBound.ID);

            try
            {
                //if bonds are overlapping
                if ((criteriaBound.ApprovalSortingAsc == false &
                     criteriaBound.BoundForApproval > criteriaBound.BoundForRejection)
                    |
                    (criteriaBound.ApprovalSortingAsc == true &
                     criteriaBound.BoundForApproval < criteriaBound.BoundForRejection))
                {
                    MipLogger.LogError("Incorrect bounds order.");
                    throw new Exception();
                }
                MipLogger.LogInfo("Leaving ValidationEngine.IsValidCriteriaBound");
            }
            catch (Exception)
            {
                MipLogger.LogError("CriteriaBound.ID: " + criteriaBound.ID + " is not valid. " + Environment.NewLine + "Leaving ValidationEngine.IsValidCriteriaBound");
                throw;
            }
        }

        /// <summary>
        /// compare list of model parameters VS parameter values for credit request (apart of constant) 
        /// </summary>
        /// <param name="creditRequest"></param>
        private void IsCreditRequestParameterValueListComplete(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered ValidationEngine.IsCreditRequestParameterValueListComplete with CreditRequestID: " + creditRequest.ID);
            try
            {
                List<long> crpvExistingList = creditRequest.CreditRequestParameterValues.
                    Where(crpv => crpv.ClientDataValue == null||crpv.ClientDataValue ==String.Empty).Select(cr => cr.ParameterID).ToList();
                List<long> crpvExpectedList = creditRequest.wSegmentCreditType.Model.Parameters.
                    Where(par => (par.ParameterTypeID == 1 || par.ParameterTypeID == 2 || par.ParameterTypeID == 3) & par.Enabled == true).Select(cr => cr.ID).ToList();

                string crpvExistingString = string.Join(",", crpvExistingList.ToArray());
                string crpvExpectedString = string.Join(",", crpvExpectedList.ToArray());

                if (!crpvExistingList.SequenceEqual(crpvExpectedList))
                {
                    MipLogger.LogError("CreditRequestParameterValue list for CreditRequest.ID: " + creditRequest.ID + " is not matching with excpected list of parameters for Model.ID: " + creditRequest.wSegmentCreditType.ModelID);
                    MipLogger.LogError("Expected list of parameters: " + crpvExpectedString + ". Existing list of parameters: " + crpvExistingString);
                    MipLogger.LogError("Leaving ValidationEngine.IsCreditRequestParameterValueListComplete");
                    throw new Exception();
                }
                ////count constants - ovo samo ukoliko se constante unose u crpv tabelu
                //var constants =
                //    creditRequest.wSegmentCreditType.Model.Parameters.Where(param => param.ParameterTypeID == 4).Select(param=>param.ID).ToList();

                //var a = (creditRequest.CreditRequestParameterValues.Where(crpv => constants.Contains(crpv.ParameterID))).Count();

            }
            catch (Exception)
            {
                MipLogger.LogError("CreditRequestParameterValue list for CreditRequest.ID: " + creditRequest.ID + " is incomplete");
                MipLogger.LogError("Leaving ValidationEngine.IsCreditRequestParameterValueListComplete");
                throw;
            }
        }


    }
}
