﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Mip2.Logger
{
    public class MipLogger
    {
        readonly static ILog logInfo = LogManager.GetLogger("Info");
        readonly static ILog logError = LogManager.GetLogger("Error");
        readonly static ILog logWarning = LogManager.GetLogger("Warning");
        readonly static ILog logVerbose = LogManager.GetLogger("Verbose");
        readonly static ILog logDb = LogManager.GetLogger("DbDebug");

        public static bool IsInfoEnabled = logInfo.IsInfoEnabled;
        public static bool IsErrorEnabled = logError.IsInfoEnabled;
        public static bool IsWarningEnabled = logWarning.IsInfoEnabled;
        public static bool IsVerboseEnabled = logVerbose.IsInfoEnabled;
        public static bool IsDbEnabled = logDb.IsInfoEnabled;


        public static void LogDb(string message)
        {
            logDb.Info(message);
        }

        public static void LogInfo(string message)
        {
            logInfo.Info(message);
        }

        public static void LogError(string message)
        {
            logError.Info(message);
        }

        public static void LogError(Exception exception)
        {
            logError.Info("Source: " + exception.Source);
            logError.Info("Target site: " + exception.TargetSite);
            logError.Info("Message: " + exception.Message);
            logError.Info("StackTrace: " + exception.StackTrace);
            logError.Info("Data: " + exception.Data);
            while (exception.InnerException != null)
            {
                LogError(exception.InnerException);
                return;
            }
        }

        public static void LogWarning(string message)
        {
            logWarning.Info(message);
        }

        public static void LogVerbose(string message)
        {
            logVerbose.Info(message);
        }
    }
}
