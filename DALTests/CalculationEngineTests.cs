﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.Ef;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace DAL.Tests
{
    [TestClass()]
    public class CalculationEngineTests
    {
        [TestMethod()]
        public void DisposeTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RunTest()
        {
            Assert.Fail();
        }
        //[TestMethod()]
        //public void SetCalculationStatusTest()
        //{
        //    CalculationEngine calculationEngine = new CalculationEngine();
        //    AccurateEntities dbContext = new AccurateEntities();
        //    var cr = dbContext.CreditRequests.FirstOrDefault();
        //    calculationEngine.SetCalculationStatus(cr,1);
        //    Assert.Fail();
        //}
        /// <summary>
        /// compare expected values 
        /// </summary>
        [TestMethod()]
        public void CalculationResultTest()
        {
            //fill list with expected values (Score and PD for creditID:7,8,9 - "PD by SALEX.xlsx")
            List<IList<decimal>> expectedList = new List<IList<decimal>>() { new List<decimal>() { Math.Round((decimal)2.010105915, 6), Math.Round((decimal)0.881854058, 6) }, new List<decimal>() { (decimal)1.98790006, Math.Round((decimal)0.879520797) }, new List<decimal>() { Math.Round((decimal)2.798220787, 6), Math.Round((decimal)0.942579603, 6) } };
            List<IList<decimal>> actualList = new List<IList<decimal>>();

            var dbContext = new AccurateEntities();
            var creditRequestList = dbContext.CreditRequests.Where(c => (new List<long>() { 7, 8, 9 }).Contains(c.ID));

            foreach (var creditRequest in creditRequestList)
            {
                creditRequest.CalculationStatusID = 1;
                dbContext.SaveChanges();
                actualList.Add(new List<decimal>() { Math.Round(creditRequest.Score.Value, 6), Math.Round(creditRequest.PD.Value, 6) });
            }

            CalculationEngine calculationEngine = new CalculationEngine();
            calculationEngine.Run();

            //compare expected VS actual
            foreach (var expected in expectedList)
            {
                foreach (var actual in actualList)
                {
                    Assert.AreEqual(expected[1], actual[1]);
                    Assert.AreEqual(expected[2], actual[2]);
                }
            }
        }

    }
}

namespace DALTests
{
    class CalculationEngineTests
    {
    }
}
