﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using DAL.Ef;
using Mip2.Logger;

namespace DAL
{
    // public class GenericDataRepository<T> : IGenericDataRepository<T> where T : class
    public class DALManager : IDisposable
    {
        #region Fields
        private AccurateEntities _context;
        #endregion

        public DALManager()
        {
            _context = new AccurateEntities();
        }
        //--GET--
        /// <summary>
        /// rerurn FirstOrDefault CreditRequest with CalculationStatusID = 1(Waiting)
        /// </summary>
        /// <param name="statusId"></param>
        /// <returns></returns>
        public CreditRequest GetFirstOrDefaultCreditRequestsByStatusId(int statusId)
        {
            ////todo sb prepunice log
            //MipLogger.LogVerbose("Entered GetterManager.GetFirstOrDefaultCreditRequestsByStatusId");

            try
            {
                //todo: MM da li je redi ispod dobar?
                var creditRequests = _context.CreditRequests.FirstOrDefault(cr => cr.CalculationStatusID == statusId);

                //todo sb prepunice log
                //MipLogger.LogInfo("Leaving GetterManager.GetFirstOrDefaultCreditRequestsByStatusId");
                return creditRequests;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retreive CreditRequest ready for calculation." + Environment.NewLine + "Leaving GetterManager.GetFirstOrDefaultCreditRequestsByStatusId");
                throw;
            }
        }
        public IList<CreditRequest> GetTopCreditRequestsByStatusId(int statusId, int limit)
        {
            ////todo sb prepunice log
            //MipLogger.LogVerbose("Entered GetterManager.GetTopCreditRequestsByStatusId");

            try
            {
                //todo: MM da li je redi ispod dobar?
                var creditRequests = _context.CreditRequests.Where(cr => cr.CalculationStatusID == statusId).Take(limit).ToList();

                ////todo sb prepunice log
                //MipLogger.LogInfo("Leaving GetterManager.GetTopCreditRequestsByStatusId");
                return creditRequests;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retreive CreditRequest ready for calculation." + Environment.NewLine + "Leaving GetterManager.GetTopCreditRequestsByStatusId");
                throw;
            }
        }
        public IList<ParameterAttribute> GetParameterAttributesForParameter(Parameter parameter)
        {
            MipLogger.LogInfo("Entered GetterManager.GetParameterAttributesForParameter with ParameterID: " + parameter.ID);

            try
            {
                //todo: MM da li ovo vraca null ako upit nista ne vraca
                var parameterAttributeList = _context.ParameterAttributes.Where(w => w.ParameterID == parameter.ID).ToList();
                //todo sb TEST value
                //var test = parameter.ParameterAttributes.Select(pa => pa);

                if (!parameterAttributeList.Any())
                {
                    throw new Exception("There are no ParameterAttributes");
                }

                MipLogger.LogInfo("Leaving GetterManager.GetParameterAttributesForParameter");
                return parameterAttributeList;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retrieve ParameterAttributes list for ParameterId: " + parameter.ID + Environment.NewLine + "Leaving GetterManager.GetParameterAttributesForParameter");
                throw;
            }
        }
        //public IList<long> GetParameterAttributeAllowedValueList(Parameter parameter)
        //  {
        //      MipLogger.LogInfo("Entered GetterManager.GetParameterAttributeAllowedValueList with ParameterID: " + parameter.ID);

        //          var allowedValueList = parameter.ParameterAttributes.Select(pa => pa.AttributeID).ToList();

        //      MipLogger.LogInfo("Leaving GetterManager.GetParameterAttributeAllowedValueList");
        //      return allowedValueList;
        //  }
        public Model GetModelForCreditRequest(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered GetterManager.GetModelForCreditRequest with CreditRequestID: " + creditRequest.ID);

            try
            {
                var model = creditRequest.wSegmentCreditType.Model;
                //sta vraca ako nema modela
                //var model = (from cr in _context.CreditRequests
                //             join wsct in _context.wSegmentCreditTypes on cr.SegmentCreditTypeID equals wsct.ID
                //             join m in _context.Models on wsct.ModelID equals m.ID
                //             where cr.ID == creditRequest.ID
                //             where wsct.Enabled == true
                //             select m).FirstOrDefault();

                if (model == null)
                {
                    MipLogger.LogError("No requested model");
                    throw new Exception();
                }

                MipLogger.LogInfo("Leaving GetterManager.GetModelForCreditRequest returning ModelID: " + model.ID);
                return model;
            }
            catch (Exception exception)
            {
                MipLogger.LogError("Unable to retreive data for Model.ID " + creditRequest.wSegmentCreditType.ModelID + exception.Message);
                throw;
            }
        }
        public IEnumerable<Parameter> GetParameterListForModel(Model model)
        {
            MipLogger.LogInfo("Entering GetterManager.GetParameterListForModel with ModelID: " + model.ID);

            //returns param list for selected model, apart of constant
            try
            {
                var parameterList = (from par in _context.Parameters
                                     where par.ModelID == model.ID
                                     where par.Enabled == true
                                     where par.ParameterTypeID == 1 || par.ParameterTypeID == 2 || par.ParameterTypeID == 3
                                     select par).ToList();

                MipLogger.LogInfo("Leaving GetterManager.GetParameterListForModel");
                return parameterList;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retreive parameters for ModelID: " + model.ID + Environment.NewLine + "Leaving GetterManager.GetParameterListForModel");
                throw;
            }
        }
        public CriteriaBound GetCriteriaBoundForCreditRequest(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered GetterManager.GetCriteriaBoundForCreditRequest with CreditRequestID: " + creditRequest.ID);

            try
            {
                var criteriaBound = creditRequest.wSegmentCreditType.CriteriaBound;
                //var criteriaBoundList = (from cr in _dbContext.CreditRequests
                //                         join wsct in _dbContext.wSegmentCreditTypes on cr.SegmentCreditTypeID equals wsct.ID
                //                         join cb in _dbContext.CriteriaBounds on wsct.CriteriaBoundID equals cb.ID
                //                         where cr.ID == creditRequest.ID
                //                         select cb).ToList();
                MipLogger.LogInfo("Leaving GetterManager.GetCriteriaBoundForCreditRequest");
                return criteriaBound;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retreive criteria bound for CreditRequest.ID" + creditRequest.ID + "Leaving GetterManager.GetCriteriaBoundForCreditRequest");
                throw;
            }
        }
        private int GetRequestStatusForPD(CreditRequest creditRequest, CriteriaBound criteriaBound, wCriteria wCriteriaItem)
        {
            MipLogger.LogInfo(string.Format(
                 "Entered GetterManager.GetRequestStatusForPD with CreditRequest.ID: {0}, CriteriaBound.ID: {1} and wCriteria.ID: {2}.",
                 creditRequest.ID, criteriaBound.ID, wCriteriaItem.ID));

            try
            {
                //dobar
                if (creditRequest.PD >= 0 & creditRequest.PD < criteriaBound.BoundForApproval)
                {
                    return 1;
                }
                //sumnjiv
                if (creditRequest.PD >= criteriaBound.BoundForApproval & creditRequest.PD < criteriaBound.BoundForRejection)
                {
                    return 3;
                }
                //zao
                if (creditRequest.PD >= criteriaBound.BoundForRejection)
                {
                    return 2;
                }
                //else
                MipLogger.LogError("Invalid PD value: " + creditRequest.PD + " for creditRequest.ID: " + creditRequest.ID);
                throw new Exception();
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to get request status for PD");
                throw;
            }
        }

        private int GetRequestStatusForScore(CreditRequest creditRequest, CriteriaBound criteriaBound, wCriteria wCriteriaItem)
        {
            MipLogger.LogInfo(string.Format(
                           "Entered GetterManager.GetRequestStatusForScore with CreditRequest.ID: {0}, CriteriaBound.ID: {1} and wCriteria.ID: {2}.",
                           creditRequest.ID, criteriaBound.ID, wCriteriaItem.ID));
            try
            {
                //if "the bigger the better"
                if (criteriaBound.ApprovalSortingAsc == true)
                {
                    //dobar
                    if (creditRequest.Score >= criteriaBound.BoundForApproval)
                    {
                        return 1;
                    }
                    //sumnjiv
                    if (creditRequest.Score >= criteriaBound.BoundForRejection & creditRequest.Score < criteriaBound.BoundForApproval)
                    {
                        return 3;
                    }
                    //zao
                    if (creditRequest.Score >= 0 & creditRequest.Score < criteriaBound.BoundForRejection)
                    {
                        return 2;
                    }
                    //else
                    MipLogger.LogInfo("Invalid score value for creditRequest.ID: " + creditRequest.ID);
                    throw new Exception();
                }

                //if "the smaller the better"
                if (criteriaBound.ApprovalSortingAsc == false)
                {
                    //dobar
                    if (creditRequest.Score >= 0 & creditRequest.Score < criteriaBound.BoundForRejection)
                    {
                        return 1;
                    }
                    //čućemo se
                    if (creditRequest.Score >= criteriaBound.BoundForApproval
                             & creditRequest.Score < criteriaBound.BoundForRejection)
                    {
                        return 3;
                    }
                    //loše
                    if (creditRequest.Score >= criteriaBound.BoundForRejection & creditRequest.Score <= 1)
                    {
                        return 2;
                    }
                    //else
                    MipLogger.LogInfo("Invalid score value for creditRequest.ID: " + creditRequest.ID);
                    throw new Exception();
                }

                //else
                MipLogger.LogError("Invalid PD value for creditRequest.ID: " + creditRequest.ID);
                throw new Exception();
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to get request status for Score");
                throw;
            }
        }

        //private wCriteria GetCriteriaForCreditRequest(CriteriaBound criteriaBound)
        //{
        //    MipLogger.LogInfo("Entered GetterManager.GetCriteriaForCreditRequest with CriteriaBound.ID: " + criteriaBound.ID);

        //    try
        //    {
        //        return criteriaBound.wCriteria;
        //    }
        //    catch (Exception)
        //    {
        //        MipLogger.LogError("Unable to retreive Criteria for CriteriaBound.ID: " + criteriaBound.ID);
        //        throw;
        //    }
        //}

        public CreditRequestParameterValue GetCreditRequestParameterValueForParameter(CreditRequest creditRequest, Parameter parameter)
        {
            MipLogger.LogInfo("Entering GetterManager.GetCreditRequestParameterValueForParameter for CreditRequestID: " + creditRequest.ID + " and ParameterID: " + parameter.ID);

            try
            {
                //must return ONE CreditRequestParameterValue for current Parameter and CreditRequestID
                var creditRequestParameterValueList = (from crpv in _context.CreditRequestParameterValues
                                                       where crpv.ParameterID == parameter.ID
                                                       where crpv.CreditRequestID == creditRequest.ID
                                                       select crpv).ToList();
                switch (creditRequestParameterValueList.Count())
                {
                    //return 0 
                    case 0:
                        MipLogger.LogError("Missing CreditRequestParameterValue");
                        throw new Exception();
                    //return 1 = correct
                    case 1:
                        break;
                    //return more than 1
                    default:
                        MipLogger.LogError("Multipple entry for CreditRequestParameterValu is detected");
                        throw new Exception();
                }

                MipLogger.LogInfo("Leavinng GetterManager.GetCreditRequestParameterValueForParameter, returning CreditRequestParameterValue.ID" + creditRequestParameterValueList[0].ID);
                return creditRequestParameterValueList[0];
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retreieve CreditRequestParameterValue for CreditRequest.ID: " + creditRequest.ID + " and ParameterID: " + parameter.ID);
                throw;
            }
        }

        public ParameterAttribute GetBelongingParameterAttribute(CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered GetterManager.GetBelongingParameterAttribute for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);

            try
            {
                var parameterAttribute = (from crpv in _context.CreditRequestParameterValues
                                          join wpa in _context.ParameterAttributes on crpv.ParameterID equals
                                              wpa.ParameterID
                                          where creditRequestParameterValue.ID == crpv.ID
                                          where crpv.Value >= wpa.LowerBound
                                          where crpv.Value < wpa.UpperBound
                                          select wpa).FirstOrDefault();
                if (parameterAttribute == null)
                {
                    MipLogger.LogError(
                        "Missing ParameterAttribute value, for Parameter with ParameterID: " + creditRequestParameterValue.ParameterID);
                    return null;
                }
                return parameterAttribute;
            }
            catch (Exception e)
            {//todo mm da li da dodam u log i exception.innerException
                MipLogger.LogError("Unable to retrieve belonging ParameterAttribute for CreditRequestParameterValue.ID: " + creditRequestParameterValue.ID);
                throw;
            }
        }

        public ParameterAttribute GetparameterAttributeForDiscreteParameterType(Parameter parameter, CreditRequestParameterValue crpv)
        {
            MipLogger.LogInfo("Entering GetterManager.GetparameterAttributeForDiscreteParameterType for Parameter.ID: " + parameter.ID + " and CreditRequestParameterValue.ID: " + crpv.ID);
            try
            {
                var singleParameterAttribute = (from wpa in _context.ParameterAttributes
                                                where wpa.ParameterID == parameter.ID
                                                where wpa.AttributeID == crpv.Value
                                                select wpa).FirstOrDefault();

                if (singleParameterAttribute == null)
                {
                    MipLogger.LogError("CreditRequestParameterValue.Value: (" + crpv.Value + ") for discrete type, can take only values from ParameterAttribute list for choosen model.");
                    throw new Exception();
                }
                return singleParameterAttribute;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retreieve parameter attribute");
                MipLogger.LogError("Leaving GetterManager.GetparameterAttributeForDiscreteParameterType");
                throw;
            }

        }

        public List<decimal?> GetparameterAttributeForDiscreteParameterType(CreditRequest creditRequest, Parameter parameter)
        {
            MipLogger.LogInfo("Entering GetterManager.GetparameterAttributeForDiscreteParameterType for CreditRequest.ID: " + creditRequest.ID + " and Parameter.ID: " + parameter.ID);
            try
            {
                var singleParameterAttributeList = (from par in _context.Parameters
                                                    join crpv in _context.CreditRequestParameterValues on par.ID equals
                                                        crpv.ParameterID into temp1
                                                    from y in temp1.DefaultIfEmpty()
                                                    join wpa in _context.ParameterAttributes on y.ParameterID equals
                                                        wpa.ParameterID
                                                    where par.ID == parameter.ID
                                                    where par.Enabled == true
                                                    where creditRequest.ID == y.CreditRequestID
                                                    select y.Value * wpa.Beta * wpa.BetaWeight).ToList();

                if (singleParameterAttributeList[0] == null)
                {
                    MipLogger.LogError("ParameterAttribute list empty");
                    throw new Exception();
                }
                MipLogger.LogInfo("Leaving GetterManager.GetparameterAttributeForDiscreteParameterType. Returning value: " + singleParameterAttributeList[0].Value);
                return singleParameterAttributeList;

            }
            catch (Exception)
            {
                MipLogger.LogInfo("Leaving GetterManager.GetparameterAttributeForDiscreteParameterType");
                throw;
            }
        }

        //constant
        /// <summary>
        ///     One model - one constant. Return it or die!
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IList<ParameterAttribute> GetConstants(Model model)
        {
            MipLogger.LogInfo("Entered GetterManager.GetConstant for Model.ID: " + model.ID);

            try
            {

                var parameterCostant = (from par in _context.Parameters.
                                            Where(p => p.ParameterTypeID == 4 & p.Enabled == true & p.ModelID == model.ID )
                                        join pa in _context.ParameterAttributes on par.ID equals pa.ParameterID into temp
                                        from temp2 in temp
                                        select temp2).ToList();
                if (!parameterCostant.Any())
                {
                    MipLogger.LogError("Missing constant for Model.ID: " + model.ID);
                    throw new Exception();
                }

                MipLogger.LogInfo("Constant found, returning value: " + parameterCostant[0]);
                return parameterCostant;

                //todo sb testiraj da li je ovde null kada je beta prazna u bazi
                //in case Beta/BetaWeight for constant are missing, use 0/1
                //return (parameterCostant[0].Beta == null ? 0 : parameterCostant[0].Beta.Value *
                //        parameterCostant[0].BetaWeight == null ? 1 : parameterCostant[0].BetaWeight.Value);
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to retrieve constant  for Model.ID: " + model.ID + Environment.NewLine + "Leaving GetterManager.GetConstant");
                throw;
            }
        }

        //public int CountConstantsForModel(Model model)
        //{
        //    MipLogger.LogInfo("Entered GetterManager.CountConstantsForModel for Model.ID: " + model.ID);

        //    try
        //    {
        //        //todo sb dodaj proveru 
        //        var countConstants = _context.Parameters.Count(param => param.ModelID == model.ID & param.Enabled == true & param.ParameterTypeID == 4 & param.ClientData!=true);
        //        MipLogger.LogInfo("Found " + countConstants + " constant(s)" + Environment.NewLine + " Leaving GetterManager.CountConstantsForModel");
        //        return countConstants;
        //        //var old = (from param in _context.Parameters
        //        //           join wpa in _context.ParameterAttributes on param.ID equals wpa.ParameterID
        //        //           where param.ModelID == model.ID & param.Enabled == true & param.ParameterTypeID == 4
        //        //           select wpa).Count();

        //        //return (from param in _context.Parameters
        //        //        join wpa in _context.ParameterAttributes on param.ID equals wpa.ParameterID
        //        //        where param.ModelID == model.ID & param.Enabled == true & param.ParameterTypeID == 4
        //        //        select wpa).Count();
        //    }
        //    catch (Exception)
        //    {
        //        MipLogger.LogError("Unable to count constants for Model.ID: " + model.ID + Environment.NewLine + "Leaving GetterManager.CountConstantsForModel");
        //        throw;
        //    }

        //}

        /// <summary>
        /// Return request status for CreditRequest, depending on Criteria (PD, Score)
        /// </summary>
        /// <param name="creditRequest"></param>
        /// <returns></returns>
        public int GetRequestStatusForCreditRequest(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered GetterManager.GetRequestStatusForCreditRequest with CreditRequestID: " + creditRequest.ID);
            try
            {
                var criteriaBound = GetCriteriaBoundForCreditRequest(creditRequest);
                var wCriteriaItem = criteriaBound.wCriteria;

                int creditRequestStatus;
                switch (wCriteriaItem.Name.ToUpper())
                {
                    case "PD":
                        creditRequestStatus = GetRequestStatusForPD(creditRequest, criteriaBound, wCriteriaItem);
                        break;
                    case "SCORE":
                        creditRequestStatus = GetRequestStatusForScore(creditRequest, criteriaBound, wCriteriaItem);
                        break;
                    //todo alex u sifrarniku stoji i Rating, sta cemo sa njim
                    default:
                        MipLogger.LogError("Invalid criteria name for CriteriaBound: " + wCriteriaItem.Name);
                        throw new Exception();
                }

                MipLogger.LogInfo("Leaving GetterManager.GetRequestStatusForCreditRequest");
                return creditRequestStatus;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to get request status for CreditRequest.ID: " + creditRequest.ID + Environment.NewLine + "Leaving GetterManager.GetRequestStatusForCreditRequest");
                throw;
            }
        }
        public void SetCalculationStatus(long creditRequestId, int statusId)
        {
            MipLogger.LogVerbose("Entered CalculationManager.SetCalculationStatus  for Creditrequest.ID: " + creditRequestId);
            try
            {
                CreditRequest creditRequest = _context.CreditRequests.FirstOrDefault(cr => cr.ID == creditRequestId);
                if (creditRequest == null)
                {
                    MipLogger.LogError("CreditRequest.ID: " + creditRequestId + " is missing");
                    throw new Exception();
                }
                creditRequest.CalculationStatusID = statusId;
                _context.SaveChanges();

                MipLogger.LogInfo("CalculationStatus: " + statusId + " successfully changed for CreditRequestID: " + creditRequest.ID
                    + Environment.NewLine + "Leaving CalculationEngine.SetCalculationStatus");



            }
            catch (Exception exception)
            {
                MipLogger.LogError("Error while trying to change CalculationStatus for CreditRequestID: " + creditRequestId +
                                    Environment.NewLine + "Leaving CalculationEngine.SetCalculationStatus" + exception.InnerException);
                throw;
            }
        }
        public void CommitCalculationResults(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered CalculationManager.CommitCalculationResults for Creditrequest.ID: " + creditRequest.ID);
            try
            {
                //calculation finished successfully
                creditRequest.CalculationStatusID = 3;

                //commit changes
                _context.SaveChanges();
                MipLogger.LogInfo("Changes successfully commited" + Environment.NewLine + "Leaving GetterManager.CommitCalculationResults");
            }
            catch (Exception ex)
            {
                //todo hendle connection errror
                MipLogger.LogError("Unable to commit changes to DB. for CreditRequest.ID: " +
                    creditRequest.ID + ex.InnerException + Environment.NewLine + "Leaving GetterManager.CommitCalculationResults");
                throw;
            }
        }

        // ref:https://msdn.microsoft.com/en-us/library/ms244737.aspx
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~DALManager()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }
    }
}
