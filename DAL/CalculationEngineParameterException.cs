﻿using System;

using DAL.Ef;

namespace DAL
{
    public class CalculationEngineParameterException : Exception
    {
        #region Contructors

        public CalculationEngineParameterException(CreditRequest creditRequest, Parameter parameter)
        {
            this.CreditRequest = creditRequest;
            this.Parameter = parameter;
        }

        #endregion

        #region Properties

        public CreditRequest CreditRequest { get; set; }

        public Parameter Parameter { get; set; }

        #endregion
    }
}