﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using DAL.Ef;
using Mip2.Logger;
using MySql.Data.MySqlClient;

namespace DAL
{
    public class CalculationEngine : IDisposable
    {
        #region Fields

        private AccurateEntities _dbContext;

        private bool _isSuccessfulCalculation = true;

        private decimal _score;

        private CreditRequest _creditRequest;

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this._dbContext.Dispose();
        }

        #endregion

        #region Mixed Members
        
        //---DAL----
        public void InvokeCalculationForCreditRequest(CreditRequest creditRequest)
        {
            MipLogger.LogInfo("Entered CalculationEngine.InvokeCalculationForCreditRequest with CreditRequestID: " + creditRequest.ID);

            try
            {
                if (IsCreditRequestValid(creditRequest))
                {
                    //todo: mm ima li smisla cuvati ovde cr u slucaju da kasnije izgubim instancu cr-a, da znam kome da setujem status na error.
                    _creditRequest = creditRequest;

                    //set creditRequest to inProgress status
                    if (!SetCalculationStatus(creditRequest, 2))
                    {
                        throw new Exception();
                    }

                    //start calculation for CreditRequest
                    if (!StartCalculation(creditRequest))
                    {
                        throw new Exception();
                    }

                    //commit previous changes
                    if (!CommitCalculationResults(creditRequest))
                    {
                        throw new Exception();

                    }
                }
                else
                {
                    //todo: mm sta ako pukne u medjuvremenu i izgubim instancu creditRequest-a? da li je sigurnije da radim undo pozivajuci globalnu _creditRequest? 
                    throw new Exception();
                }

                // next creditRequest
            }

            catch (Exception caluclationException)
            {
                if (caluclationException.InnerException is MySqlException)
                {
                    //TODO: MM handle network disconection (etc try for 15 sec then quit)
                    MipLogger.LogError(caluclationException.InnerException);
                }
                else
                {
                    MipLogger.LogError(caluclationException.InnerException);
                }

                MipLogger.LogError("Calculation for CreditRequestId: " + creditRequest.ID + " has been stopped.");

                //undo, set creditRequest status to error
                SetCalculationStatus(creditRequest, 4);
            }
            MipLogger.LogInfo("Leaving CalculationEngine.InvokeCalculationForCreditRequest");
        }

        private bool CommitCalculationResults(CreditRequest creditRequest)
        {
            try
            {
                if (_isSuccessfulCalculation)
                {
                    //add Constant to score
                    creditRequest.Score = _score;

                    //calculation Finished
                    creditRequest.CalculationStatusID = 3;

                    //commit all previous changes
                    _dbContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                //todo hendle connection errror
                MipLogger.LogError("Unable to commit changes for CreditRequest.ID: " + creditRequest.ID);
                return false;
            }
            return true;
        }

       
        /// <summary>
        ///     set calc status for single credit request using current dbContext
        /// </summary>
        /// <param name="creditRequest"></param>
        /// <param name="statusId"></param>
        private bool SetCalculationStatus(CreditRequest creditRequest, int statusId)
        {
            MipLogger.LogVerbose("Entered CalculationEngine.SetCalculationStatus");

            try
            {
                creditRequest.CalculationStatusID = statusId;
                _dbContext.SaveChanges();

                MipLogger.LogInfo("CalculationStatus: " + statusId + " has been successfully changed for CreditRequestID: " + creditRequest.ID);
                MipLogger.LogVerbose("Leaving CalculationEngine.SetCalculationStatus");
                return true;
            }
            catch (Exception exception)
            {
                MipLogger.LogError("Error while trying to change CalculationStatus for CreditRequestID: " + creditRequest.ID +
                    exception.InnerException);
                return false;
            }
        }


      

        //---CALCULATION---
        public bool CalculateScoreForParameterList(CreditRequest creditRequest, IEnumerable<Parameter> parameterList)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateScoreForParameterList with CreditRequestID: " + creditRequest.ID);

            try
            {
                foreach (var parameter in parameterList)
                {
                    //if parameter is valid
                    if (IsParameterValid(parameter))
                    {
                        //returns CreditRequestParameterValue for current Parameter and CreditRequestID
                        var creditRequestParameterValue = GetCreditRequestParameterValueForParameter(creditRequest, parameter);

                        //recalc value for CreditRequestParameterValue

                        _score = _score + GetParameterRecalculatedValue(creditRequest, parameter, creditRequestParameterValue);

                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                MipLogger.LogInfo("Score (apart of constant) for CreditRequestID: " + creditRequest.ID + " is successfully calculated. Value is: " + _score);
                MipLogger.LogInfo("Leaving CalculationEngine.CalculateScoreForParameterList.");
                return true;
            }
            catch (Exception)
            {
                MipLogger.LogError("Score calculation for CreditRequestID: " + creditRequest.ID + " has been stopped.");
                _isSuccessfulCalculation = false;
                throw new Exception();
            }
        }

        public bool CalculateRequestStatusForCreditRequest(CreditRequest creditRequest)
        {
            try
            {
                var requestStatus = GetRequestStatusForCreditRequest(creditRequest);
                if (requestStatus == -1)
                {
                    throw new Exception();
                }

            }
            catch (Exception)
            {
                MipLogger.LogError("Calculating request status for CreditRequest.ID: " + creditRequest.ID + " has been stopped.");
                return false;
            }
            //var wCriteriasList = (from cr in this._dbContext.CreditRequests
            //                      join wsct in this._dbContext.wSegmentCreditTypes on cr.SegmentCreditTypeID equals wsct.ID
            //                      join cb in this._dbContext.CriteriaBounds on wsct.CriteriaBoundID equals cb.ID
            //                      join wc in this._dbContext.wCriterias on cb.CriteriaID equals wc.ID
            //                      where cr.ID == creditRequest.ID
            //                      where wsct.Enabled == true
            //                      select wc).ToList();



            return true;
        }


        public decimal CalculateRecalculatedValueForDiscreteParameter(Parameter parameter, CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered CalculationEngine.GetRecalculatedValueForDiscreteParameter for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
            try
            {
                //todo sb ako upit nista ne vraca da li onda dobijam null? 
                {
                    var singleParameterAttribute = (from wpa in this._dbContext.ParameterAttributes
                                                    where wpa.ParameterID == parameter.ID
                                                    where wpa.AttributeID == creditRequestParameterValue.Value
                                                    select wpa).FirstOrDefault();

                    if (singleParameterAttribute == null)
                    {
                        MipLogger.LogError("Missing ParameterAttribute");
                        throw new Exception();
                    }
                    //return RecalculatedValue
                    var result = (singleParameterAttribute.Beta.Value * singleParameterAttribute.BetaWeight.Value); //Beta and BetaWeight are already checked for not to be null
                    MipLogger.LogInfo("Leaving CalculationEngine.GetRecalculatedValueForDiscreteParameter. Returning value: " + result);
                    return result;
                }
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate recalculated value for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
                return -1;
            }
        }

        public decimal CalculateRecalculatedValueForDiscretizedContinuousParameter(CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateRecalculatedValueForDiscretizedContinuousParameter for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
            try
            {
                {
                    var singleParameterAttribute = GetBelongingParameterAttribute(creditRequestParameterValue);

                    //return RecalculatedValue
                    var result = (singleParameterAttribute.Beta.Value * singleParameterAttribute.BetaWeight.Value); //Beta and BetaWeight are already checked for not to be null
                    MipLogger.LogInfo("Leaving CalculationEngine.GetRecalculatedValueForDiscreteParameter. Returning value: " + result);
                    return result;
                }
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate recalculated value for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
                return -1;
            }
        }
        public decimal CalculateRecalculatedValueForContinuousParameter(CreditRequest creditRequest, Parameter parameter, CreditRequestParameterValue creditRequestParameterValue)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateRecalculatedValueForContinuousParameter for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
            try
            {
                {
                    //TODO probaj da sredis upit zato sto ovako mesa left outer i inner join.

                    var singleParameterAttributeList = (from par in _dbContext.Parameters
                                                        join crpv in _dbContext.CreditRequestParameterValues on par.ID equals
                                                            crpv.ParameterID into temp1
                                                        from y in temp1.DefaultIfEmpty()
                                                        join wpa in _dbContext.ParameterAttributes on y.ParameterID equals
                                                            wpa.ParameterID
                                                        where par.ID == parameter.ID
                                                        where par.Enabled == true
                                                        where creditRequest.ID == y.CreditRequestID
                                                        select y.Value * wpa.Beta * wpa.BetaWeight).ToList();
                    //must return one row
                    switch (singleParameterAttributeList.Count())
                    {
                        case 0:
                            MipLogger.LogError("Missing ParameterAttribute");
                            throw new Exception();
                        case 1:
                            break;
                        default:
                            MipLogger.LogError("Multiple entry for ParameterAttribute");
                            throw new Exception();
                    }

                    //return RecalculatedValue
                    var result = singleParameterAttributeList[0].Value;
                    MipLogger.LogInfo("Leaving CalculationEngine.CalculateRecalculatedValueForContinuousParameter. Returning value: " + result);
                    return result;
                }
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate recalculated value for CreditRequestParameterValueID: " + creditRequestParameterValue.ID);
                return -1;
            }
        }
        public decimal CalculateProbabilityOfDefault(decimal score)
        {
            MipLogger.LogInfo("Entered CalculationEngine.CalculateProbabilityOfDefault for score: " + score);

            try
            {
                //math.exp prima samo double vrednost, zato se radi convert ToDouble pa covert ToDecimal
                var PD = 1 / (1 + Convert.ToDecimal(Math.Exp(-Convert.ToDouble(score))));
                MipLogger.LogInfo("Leaving CalculationEngine.CalculateProbabilityOfDefault, returning value: " + PD);
                return PD;
            }
            catch (Exception)
            {
                MipLogger.LogError("Unable to calculate PD for score: " + score);
                throw new Exception();
            }
        }


       

        #endregion
    }
}